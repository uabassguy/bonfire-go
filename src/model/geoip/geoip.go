package geoip

import (
	"github.com/nranchev/go-libGeoIP"
)

func CountryByIp(ip string) string {

	// Set the arguments
	dbFile := "GeoIP.dat"
	ipAddr := ip

	// Load the database file, exit on failure
	gi, err := libgeo.Load(dbFile)
	if err != nil {
		return err.Error()
	}

	// Lookup the IP and return country code
	loc := gi.GetLocationByIP(ipAddr)
	if loc != nil {
		return loc.CountryCode
		/*fmt.Printf("Country: %s (%s)\n", loc.CountryName, loc.CountryCode)
		fmt.Printf("City: %s\n", loc.City)
		fmt.Printf("Region: %s\n", loc.Region)
		fmt.Printf("Postal Code: %s\n", loc.PostalCode)
		fmt.Printf("Latitude: %f\n", loc.Latitude)
		fmt.Printf("Longitude: %f\n", loc.Longitude)*/
	}
	return "undefined"
}
