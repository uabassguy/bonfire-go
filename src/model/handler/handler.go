package handler

import (
	"fmt"
	"github.com/astaxie/beego/session"
	"html/template"
	"io/ioutil"
	"model/block"
	"model/dbmap"
	"model/urlconfig"
	"net/http"
)

var globalSessions *session.Manager

func init() {
	globalSessions, _ = session.NewManager("file", `{"cookieName":"gosessionid","gclifetime":3600,"ProviderConfig":"./var/session"}`)
	go globalSessions.GC()
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Couldn't find the page")
}

func Default(w http.ResponseWriter, r *http.Request) {

	sess, _ := globalSessions.SessionStart(w, r)
	defer sess.SessionRelease(w)
	email := sess.Get("email")

	var path string
	var loggedin string

	if email != nil {
		loggedin = "loggedin"
		path = "view/defaultloggedin.html"
	} else {
		path = "view/default.html"
	}

	filetext, _ := ioutil.ReadFile(path)

	t, _ := template.New("default_template").Parse(string(filetext))

	title := "Bonfire"

	params := map[string]interface{}{
		"Title":          title,
		"ToplinkAccount": "Login",
		"BlockHead":      template.HTML(block.HeadFoot("head"+loggedin, title)),
		"BlockFoot":      template.HTML(block.HeadFoot("foot"+loggedin, "")),
		"BaseUrl":        urlconfig.BaseUrl(),
		"LoginUrl":       urlconfig.LoginUrl(),
		"RegisterUrl":    urlconfig.RegisterUrl(),
		"LogoutUrl":      urlconfig.LogoutUrl(),
		"FirstName":      email,
		"ProfileUrl":     urlconfig.ProfileUrl(),
		"SettingsUrl":    urlconfig.SettingsUrl(),
		"PagesUrl":       urlconfig.PagesUrl(),
		"CommunityUrl":   urlconfig.CommunityUrl(),
		"FriendsUrl":     urlconfig.FriendsUrl(),
	}
	t.Execute(w, params)
}

func FileServe(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, r.URL.Path[1:])
}

func AdminHandler(w http.ResponseWriter, r *http.Request) {
	sess, _ := globalSessions.SessionStart(w, r)
	defer sess.SessionRelease(w)
	uid := sess.Get("uid")

	var validated bool
	validated = false

	if uid != nil {
		validated = dbmap.ValidateAdmin(uid.(int))
	}

	var path string
	var loggedin string
	var email string

	if validated {
		loggedin = "loggedin"
		path = "view/admin/index.html"
	} else {
		path = "view/admin/login.html"
	}

	filetext, _ := ioutil.ReadFile(path)

	t, _ := template.New("default_template").Parse(string(filetext))

	title := "Bonfire Admin"

	content := template.HTML(block.HeadFoot("admin/pages/dash", title))

	params := map[string]interface{}{
		"Title":     title,
		"BlockHead": template.HTML(block.HeadFoot("admin/head"+loggedin, title)),
		"BlockFoot": template.HTML(block.HeadFoot("admin/foot"+loggedin, "")),
		"BaseUrl":   urlconfig.BaseUrl(),
		"LoginUrl":  urlconfig.AdminLoginUrl(),
		"LogoutUrl": urlconfig.LogoutUrl(),
		"FirstName": email,
		"Content":   content,
	}
	t.Execute(w, params)
}
