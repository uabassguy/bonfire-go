package api

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/session"
	"log"
	"model/dbmap"
	"model/status"
	"model/urlconfig"
	"net/http"
)

type Credentials struct {
	Email    string "json:email"
	Password string "json:password"
}

type Response struct {
	StatusInt int
	StatusMsg string
}

var globalSessions *session.Manager

func init() {
	globalSessions, _ = session.NewManager("file", `{"cookieName":"gosessionid","gclifetime":3600,"ProviderConfig":"./var/session"}`)
	go globalSessions.GC()
}

func GetResponseCode(i int) Response {
	Out := Response{}
	Out.StatusInt = i
	Out.StatusMsg = status.Code(i)

	return Out
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "POST")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")

	User := Credentials{}
	User.Email = r.FormValue("email")
	User.Password = r.FormValue("password")

	uid, returncode := dbmap.PasswordValidation(w, r)
	if uid > 0 {
		sess, _ := globalSessions.SessionStart(w, r)
		defer sess.SessionRelease(w)
		sess.Set("email", r.Form["email"])
		sess.Set("uid", uid)
	}

	Out := GetResponseCode(returncode)

	output, _ := json.Marshal(Out)

	if Out.StatusInt == 10 {
		log.Println(User.Email + " Logged in")
	} else {
		log.Println(Out.StatusMsg)
	}

	fmt.Fprintf(w, string(output))
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "POST")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")

	status := dbmap.CreateUser(w, r)

	if status == 20 {
		uid, returncode := dbmap.PasswordValidation(w, r)
		status = returncode
		sess, _ := globalSessions.SessionStart(w, r)
		defer sess.SessionRelease(w)
		sess.Set("email", r.Form["email"])
		sess.Set("uid", uid)
		log.Printf("%s registered", r.Form["email"])
	}
	Out := GetResponseCode(status)

	output, _ := json.Marshal(Out)

	fmt.Fprintf(w, string(output))
}

func ForgetUser(w http.ResponseWriter, r *http.Request) {
	globalSessions.SessionDestroy(w, r)
	http.Redirect(w, r, urlconfig.BaseUrl(), 302)
}
