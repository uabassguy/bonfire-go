package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"io/ioutil"
	"log"
	"model/dbmap"
	"model/urlconfig"
	"net/http"
	"strconv"
	"strings"
)

func GetAdmin(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "POST")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")

	User := Credentials{}
	User.Email = r.FormValue("email")
	User.Password = r.FormValue("password")

	uid, returncode := dbmap.AdminPasswordValidation(w, r)
	if uid > 0 {
		sess, _ := globalSessions.SessionStart(w, r)
		defer sess.SessionRelease(w)
		sess.Set("email", r.Form["email"])
		sess.Set("uid", uid)
	}

	Out := GetResponseCode(returncode)

	output, _ := json.Marshal(Out)

	if Out.StatusInt == 30 {
		log.Println("Admin " + User.Email + " Logged in")
	} else {
		log.Println(Out.StatusMsg)
	}

	fmt.Fprintf(w, string(output))
}

func GetEntity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "POST")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	sess, _ := globalSessions.SessionStart(w, r)
	defer sess.SessionRelease(w)
	uid := sess.Get("uid")

	var validated bool
	validated = false

	if uid != nil {
		validated = dbmap.ValidateAdmin(uid.(int))
	}

	var path string

	if validated {
		path = "view/admin/block/grid.html"
	} else {
		path = "view/admin/login.html"
	}

	entityname := mux.Vars(r)["entity"]

	filetext, _ := ioutil.ReadFile(path)

	t, _ := template.New("grid_template").Parse(string(filetext))

	params := map[string]interface{}{
		"BaseUrl":    urlconfig.BaseUrl(),
		"EntityName": strings.Title(entityname),
	}

	switch entityname {
	case "attribute":
		datamap := dbmap.GetAllAttributes()
		params["AttributeMap"] = datamap
		params["GridTitle"] = "All Attributes"
	default:
		datamap := dbmap.GetAllEntities()
		params["EntityMap"] = datamap
		params["GridTitle"] = "All Entities"
	}

	t.Execute(w, params)

}

func GetEditEntity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "POST")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	sess, _ := globalSessions.SessionStart(w, r)
	defer sess.SessionRelease(w)
	uid := sess.Get("uid")

	var validated bool
	validated = false

	if uid != nil {
		validated = dbmap.ValidateAdmin(uid.(int))
	}

	var path string

	if validated {
		path = "view/admin/block/edit.html"
	} else {
		path = "view/admin/login.html"
	}

	//eid :=
	eidint, _ := strconv.Atoi(mux.Vars(r)["id"])

	filetext, _ := ioutil.ReadFile(path)

	t, _ := template.New("grid_template").Parse(string(filetext))

	params := map[string]interface{}{
		"BaseUrl":    urlconfig.BaseUrl(),
		"EntityName": strings.Title("entityname"),
	}

	datamap := dbmap.GetEntityAttributes(eidint)
	attrmap := dbmap.GetAllAttributes()
	params["EntityMap"] = datamap
	params["AttributeMap"] = attrmap
	params["GridTitle"] = "entitynametitle"

	t.Execute(w, params)

}
