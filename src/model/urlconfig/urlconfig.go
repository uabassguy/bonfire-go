package urlconfig

func BaseUrl() string {
	return string("http://localhost:8080")
}

func LoginUri() string {
	return "/api/user/login"
}

func LoginUrl() string {
	return string(BaseUrl() + LoginUri())
}

func LogoutUrl() string {
	return string(BaseUrl() + LogoutUri())
}

func LogoutUri() string {
	return "/api/user/logout"
}

func AdminLoginUri() string {
	return "/api/user/admin/login"
}

func AdminLoginUrl() string {
	return string(BaseUrl() + AdminLoginUri())
}

func AdminUrl() string {
	return string(BaseUrl() + AdminUri())
}

func AdminUri() string {
	return "/admin"
}

func Uri(u string) string {
	return "/" + u
}
func Url(u string) string {
	return BaseUrl() + Uri(u)
}

func ProfileUrl() string {
	return string(BaseUrl() + ProfileUri())
}

func ProfileUri() string {
	return "/u"
}

func PagesUrl() string {
	return string(BaseUrl() + PagesUri())
}

func PagesUri() string {
	return "/p"
}

func FriendsUrl() string {
	return string(BaseUrl() + FriendsUri())
}

func FriendsUri() string {
	return "/f"
}

func CommunityUrl() string {
	return string(BaseUrl() + CommunityUri())
}

func CommunityUri() string {
	return "/c"
}

func SettingsUrl() string {
	return string(BaseUrl() + SettingsUri())
}

func SettingsUri() string {
	return "/settings"
}

func RegisterUri() string {
	return "/api/user/create"
}

func RegisterUrl() string {
	return string(BaseUrl() + RegisterUri())
}

func BrowseUserUri() string {
	return "/u"
}

func MainUri() string {
	return "/"
}
