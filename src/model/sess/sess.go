package sess

import (
	"github.com/astaxie/beego/session"
)

var globalSessions *session.Manager

func init() globalSessions {
	globalSessions, _ = session.NewManager("file", `{"cookieName":"gosessionid","gclifetime":3600,"ProviderConfig":"./var/session"}`)
	go globalSessions.GC()
	return globalSessions
}
