package server

import (
	"github.com/gorilla/mux"
	"model/api"
	"model/handler"
	"model/urlconfig"
	"net/http"
)

func Start() {

	routes := mux.NewRouter()
	routes.HandleFunc(urlconfig.RegisterUri(), api.CreateUser).Methods("POST")
	routes.HandleFunc(urlconfig.LoginUri(), api.GetUser).Methods("POST")
	routes.HandleFunc(urlconfig.AdminLoginUri(), api.GetAdmin)                    //.Methods("POST")
	routes.HandleFunc(urlconfig.LogoutUri(), api.ForgetUser)                      //.Methods("POST")
	routes.HandleFunc(urlconfig.Uri("admin/ajax/entity/{entity}"), api.GetEntity) //.Methods("POST")
	routes.HandleFunc(urlconfig.Uri("admin/ajax/edit/entity/{id}"), api.GetEditEntity)
	//routes.HandleFunc(urlconfig.MainUri(), handler.DefaultLoggedIn)
	routes.HandleFunc(urlconfig.AdminUri(), handler.AdminHandler)
	routes.NotFoundHandler = http.HandlerFunc(handler.NotFound)
	routes.HandleFunc("/", handler.Default)

	http.HandleFunc("/assets/", handler.FileServe)
	http.Handle("/", routes)
	http.ListenAndServe(":8080", nil)
}
