package block

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"model/urlconfig"
)

func HeadFoot(f string, ti string) string {

	filetext, _ := ioutil.ReadFile("view/" + f + ".html")

	t, _ := template.New(f + "_block").Parse(string(filetext))

	params := map[string]interface{}{
		"BaseUrl": urlconfig.BaseUrl(),
		"Title":   ti,
	}

	var doc bytes.Buffer
	t.Execute(&doc, params)
	s := doc.String()

	return s
}

func Block(f string, ti string) string {

	filetext, _ := ioutil.ReadFile("view/" + f + ".html")

	t, _ := template.New(f + "_block").Parse(string(filetext))

	params := map[string]interface{}{
		"BaseUrl": urlconfig.BaseUrl(),
		"Title":   ti,
	}

	var doc bytes.Buffer
	t.Execute(&doc, params)
	s := doc.String()

	return s
}
