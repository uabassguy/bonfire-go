package xmlconfig

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

type Database struct {
	User     string `xml:"User"`
	Pass     string `xml:"Pass"`
	Host     string `xml:"Host"`
	Database string `xml:"Database"`
}

var v Database

func GetCredentials() (User, Pass, Host, Database string) {
	User = v.User
	Pass = v.Pass
	Host = v.Host
	Database = v.Database
	return User, Pass, Host, Database
}

func init() {

	xmlFile, err := os.Open("./var/conf/conf.xml")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer xmlFile.Close()

	b, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		fmt.Println("Unable to read config: %v", err)
		return
	}

	xml.Unmarshal(b, &v)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
}
