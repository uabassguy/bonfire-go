package status

import (
	"model/urlconfig"
)

func Code(c int) string {
	switch c {

	case 1:
		return "OK"
	case 10:
		return urlconfig.MainUri() //logged in, redirect
	case 11:
		return "Email not found"
	case 12:
		return "Email or Password was incorrect"
	case 20:
		return urlconfig.MainUri() //registered, redirect
	case 21:
		return "Failed to Register. Email already exists"
	case 30:
		return urlconfig.AdminUri() //logged in, redirect
	case 32:
		return "Email or Password was incorrect"
	case 99:
		return "Database connection failed"
	}

	return "Unknown error " + string(c)
}
