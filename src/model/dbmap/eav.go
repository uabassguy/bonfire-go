package dbmap

import (
	"fmt"
	"strings"
)

type Entity struct {
	Id   int
	Name string
	//Attributes string has value (map of Attribute.Id)
	//example: 2,7,321
	Attributes string
}

type Attribute struct {
	Id     int //referenced by map of Entity{Attributes:[2,7,321]}
	Name   string
	Typeof string
}

type Value struct {
	AttributeId int //xref Attribute.Id
	Id          int //relative to unit, e.g. "Post" Entity with Id 1, auto increment
	Text        string
}

type Attributes map[string]interface{}
type Values map[string]interface{}

//fetch all Attributes from the database
func GetAllEntities() map[int]Entity {
	dbmap := initDb()
	defer dbmap.Db.Close()

	var iface []Entity
	_, err := dbmap.Select(&iface, "select * from eav_entity order by Id")
	if err != nil {
		fmt.Println("Could not get all entities")
	}

	e := make(map[int]Entity)
	for k, v := range iface {
		e[k] = Entity{v.Id, v.Name, v.Attributes}
	}

	return e
}

//fetch a single Entity from the database by Id
func GetEntity(i int) *Entity {
	dbmap := initDb()
	defer dbmap.Db.Close()

	eget, _ := dbmap.Get(Entity{}, i)
	e := eget.(*Entity)
	return e
}

//fetch all attributes from the database
func GetAllAttributes() map[int]Attribute {
	dbmap := initDb()
	defer dbmap.Db.Close()

	var iface []Attribute
	_, err := dbmap.Select(&iface, "select * from eav_attribute order by Id")
	if err != nil {
		fmt.Println("Could not get all entities")
	}

	a := make(map[int]Attribute)
	for k, v := range iface {
		a[k] = Attribute{v.Id, v.Name, v.Typeof}
	}

	return a
}

//fetch a single attribute from the database by Id
func GetAttribute(i int) *Attribute {
	dbmap := initDb()
	defer dbmap.Db.Close()

	aget, _ := dbmap.Get(Attribute{}, i)
	a := aget.(*Attribute)
	return a
}

//get all attributes associated with Entity
func GetEntityAttributes(i int) map[int]Attribute {
	e := GetEntity(i)
	ids := strings.Replace(e.Attributes, " ", ", ", -1)

	dbmap := initDb()
	defer dbmap.Db.Close()

	var attrs []Attribute
	_, err := dbmap.Select(&attrs, "select * from eav_attribute where id in ("+ids+") order by Id")

	if err != nil {
		fmt.Println("Cannot read entities from database")
	}

	a := make(map[int]Attribute)

	for k, v := range attrs {
		a[k] = Attribute{v.Id, v.Name, v.Typeof}
	}

	return a
}

func GetValues(a map[int]Attribute) map[int]Value {

	v := make(map[int]Value)

	for _, val := range a {
		v[val.Id] = Value{val.Id, 1, "somevalue"}
	}

	return v
}
