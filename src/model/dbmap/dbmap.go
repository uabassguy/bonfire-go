package dbmap

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
	"log"
	stat "model/status"
	"model/xmlconfig"
	"net/http"
	"time"
)

func newPost(title, body string, uid string) Post {
	return Post{
		Created: time.Now().UnixNano(),
		Title:   title,
		Body:    body,
		Userid:  uid,
	}
}

func newUser(username, email, first, last, password string) User {
	return User{
		Name:     username,
		Email:    email,
		First:    first,
		Last:     last,
		Password: password,
	}
}

func checkUser(email, password string) User {
	return User{
		Email:    email,
		Password: password,
	}
}

func initDb() *gorp.DbMap {
	// connect to db using standard Go database/sql API
	// use whatever database/sql driver you wish
	user, pass, _, database := xmlconfig.GetCredentials()
	db, err := sql.Open("mysql", user+":"+pass+"@/"+database)
	checkErr(err, "sql.Open failed")

	// construct a gorp DbMap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}

	// add a table, setting the table name to 'post' and
	// specifying that the Id property is an auto incrementing PK
	dbmap.AddTableWithName(Post{}, "post").SetKeys(true, "Id")
	dbmap.AddTableWithName(User{}, "user").SetKeys(true, "Id")
	dbmap.AddTableWithName(Admin{}, "admin").SetKeys(true, "Id")
	dbmap.AddTableWithName(Entity{}, "eav_entity").SetKeys(true, "Id")
	dbmap.AddTableWithName(Attribute{}, "eav_attribute").SetKeys(true, "Id")
	dbmap.AddTableWithName(Value{}, "eav_value").SetKeys(true, "Id")

	// create the table. in a production system you'd generally
	// use a migration tool, or create the tables via scripts
	err = dbmap.CreateTablesIfNotExists()
	checkErr(err, "Create tables failed")

	return dbmap
}

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

//Create new user
func CreateUser(w http.ResponseWriter, r *http.Request) int {
	username := r.FormValue("user")
	email := r.FormValue("email")
	first := r.FormValue("first")
	last := r.FormValue("last")
	password := r.FormValue("password")

	var status int
	dbmap := initDb()
	defer dbmap.Db.Close()

	user := newUser(username, email, first, last, password)
	err := dbmap.Insert(&user)

	if err != nil {
		status = 21
	} else {
		status = 20
	}
	logmsg := stat.Code(status)
	log.Printf(logmsg)

	return status
}

//Password validation using dbmap model
func PasswordValidation(w http.ResponseWriter, r *http.Request) (id int, s int) {

	email := r.FormValue("email")
	password := r.FormValue("password")

	var status int
	dbmap := initDb()
	defer dbmap.Db.Close()

	user := checkUser(email, password)
	err := dbmap.SelectOne(&user, "SELECT * FROM user WHERE Email=? AND Password=?", email, password)

	switch {
	case err == sql.ErrNoRows:
		status = 12
	case err != nil:
		status = 12
	default:
		status = 10
		id = user.Id
	}
	logmsg := stat.Code(status)
	log.Printf(logmsg)

	return id, status
}

//Admin Password validation using crud model
func AdminPasswordValidation(w http.ResponseWriter, r *http.Request) (id int, s int) {

	email := r.FormValue("email")
	password := r.FormValue("password")

	var status int

	dbmap := initDb()
	defer dbmap.Db.Close()

	user := checkUser(email, password)
	err := dbmap.SelectOne(&user, "SELECT user.id FROM user, admin WHERE user.Email=? AND user.Password=? AND user.Id=admin.Id", email, password)

	switch {
	case err == sql.ErrNoRows:
		status = 32
	case err != nil:
		status = 32
	default:
		status = 30
	}
	logmsg := stat.Code(status)
	log.Printf(logmsg)

	return id, status
}

func ValidateAdmin(id int) bool {
	dbmap := initDb()
	defer dbmap.Db.Close()

	obj, err := dbmap.Get(Admin{}, id)

	switch {
	case obj == nil:
		log.Println("obj was nil")
		return false
	case err != nil:
		return false
		log.Fatal(err)
	default:
		return true
	}

	return false
}
