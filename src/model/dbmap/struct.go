package dbmap

type Post struct {
	// db tag lets you specify the column name if it differs from the struct field
	Id      int64
	Created int64
	Title   string
	Body    string
	Userid  string
}

type User struct {
	Id       int
	Name     string
	Email    string
	First    string
	Last     string
	Password string
}

type Admin struct {
	Id int
}
