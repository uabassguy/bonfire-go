$(".form-handler").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");

    $.ajax({
        url: formURL,
        type: 'post',
        dataType: 'json',
        data: postData,
        success: function (data) {
            if(data.StatusInt == 10 || data.StatusInt == 20 || data.StatusInt == 30) {
				window.location.href = data.StatusMsg;
			} else {
				alert(data.StatusMsg);
			}
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert( "Failed: " + textStatus );    
        }
    });
    
    e.preventDefault(); //STOP default action
    //e.unbind(); //unbind. to stop multiple form submit.
});