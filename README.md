# bonfire-go #

### What is bonfire-go? ###

* A basic MVC platform built in go, with support for templates, an api, mysql as the db backend, hopefully NoSql support in the future (once I feel the ORM model is solid enough)

### How do I get set up? ###

* Create a mysql database and add config in var/conf/conf.xml
* Use go get to install any dependencies in dependencies.txt
* Run main.go, then browse it at port 8080
* Use the /admin uri to access the admin interface 
* To add an admin, you'll have to designate the user id in the admin users table after creating the user on the frontend. This allows single sign on for both front and backend, I'll build a mechanism to handle this in the UI later